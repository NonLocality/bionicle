# README #

Discord: https://discord.gg/dZP876E
Trello: https://trello.com/b/NZZfMDm5/bionicle-mop-roadmap

This README documents the steps are necessary to get access to Bionicle: Masks of Power

### What is Bionicle: Masks of Power? ###

Bionicle: Masks of Power is a fan-made, fan-driven game based on the 2001 storyline of the Lego franchise, Bionicle. It's an homage to the the original cancelled 2001 game,
and a way for fans of the franchise to connect and discuss Bionicle. The Project is free and open-source, and is being produced without funds or intention of making profit. 
Bionicle as a franchise was cancelled this year, but we aim to keep it alive.

### How do I get set up? ###

It is recommended you download and install SourceTree
It is *Required* that you download and install Git LFS, available here: https://git-lfs.github.com/
Git LFS allows the project to be pushed and pulled at a much faster rate, beyond the 2gb size limit for Repos This is non-negotiable. If you do not have it installed, then you will not be able to access the files, just the LFS pointers. 
Once you have LFS installed, you may clone the project and go from there at your liesure.

If you are a BMoP Developer, there are additional steps you must take before pushing any changes to the repo.
1: Under the Repository Tab on your Git GUI (GitHub Desktop, Sourcetree), under Git LFS, click "Track/Untrack files".
2: Add: umap, uasset, and uproject to this list (no period)
3: You must do this before adding any new content to the project. If you do not do this step the Repo will bloat and lock up over time.
4: After this has been done, any new files added to the project will be pushed to the LFS server.

That should be all you need to do. If you have issues, contact us on the discord: https://discord.gg/dZP876E

### Contribution guidelines ###

If you are not on the team and would like to contribute, or have already cloned the project and made modifications you'd like to see added, please be sure to stick with Blueprints.

### Who do I talk to? ###

Contact us on our discord: https://discord.gg/dZP876E
Ask for a member of the Dev team, or the project Creator, Jocool.

### Copyright Notice ###
Bionicle is a registered property of the Lego Company (2000-2017), and we claim no right of ownership to it, nor do we have any intention to marketize it, sell it, or otherwise produce a market substitute for any current Lego property.